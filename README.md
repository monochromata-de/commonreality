# CommonReality

[![pipeline status](https://gitlab.com/de.monochromata/commonreality/badges/master/pipeline.svg)](https://gitlab.com/de.monochromata/commonreality/commits/master)

An intersubjective reality used in jACT-R cognitive models (www.jactr.org).

The Maven site is located at http://monochromata.de/maven/sites/org.commonreality/

Artifacts are published to the [Maven repository]( http://monochromata.de/maven/releases/org.commonreality/) and
the [p2 (Eclipse) update site](http://monochromata.de/eclipse/sites/org.commonreality/).

TODO: Move all published artifacts to jactr.org and Maven central, The check on Travis CI should be based on https://github.com/amharrison/commonreality or a jACT-R Team repository.
